# snowball

[Snowball](https://snowballstem.org/) is a small string processing language
for creating stemming algorithms for use in Information Retrieval, plus a
collection of stemming algorithms implemented using it.

Snowball was originally designed and built by Martin Porter.  Martin retired
from development in 2014 and Snowball is now maintained as a community
project.  Martin originally chose the name Snowball as a tribute to SNOBOL,
the excellent string handling language from the 1960s.  It now also serves as
a metaphor for how the project grows by gathering contributions over time.
